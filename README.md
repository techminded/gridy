# Gridy Grid

Web Components based data grid (table).

## Features

* W3C browser standards and native modern technologies compilant
* Modular and flexible to use and extend for any complexity level
* Can be bootstrapped completely from markup as far as from js code (dynamic render)
* Skinnable and templatable UI, can mimic to framework of your choice
* Modern code style with no transpilations required, but you are still free to do that
* Options to have wide browser support (including legacy browsers like IE11)

If you are looking for web components based widgets collection check out [skinny-widgets](https://www.npmjs.com/package/skinny-widgets) project.


## Simple Usage

Data is loaded from json within javascript context.

```html
<gridy-table base-path="/node_modules/gridy-grid/src" theme="antd" id="gridyTable"></gridy-table>

<script type="module">
    import { GridyTable } from '/node_modules/gridy-grid/src/table/gridy-table.js';
    import { DataSourceLocal } from '/node_modules/gridy-grid/src/datasource/data-source-local.js';
    let dataSource = new DataSourceLocal();
    dataSource.fields = [
        { title: 'Title', path: '$.title' },
        { title: 'Price', path: '$.price' }
    ];
    let data = [];
    for (let i = 0; i < 10; i++) {
        data.push({ title: 'row' + i, price: 100 * i })
    }
    // local datasource data load should be called explicitly
    gridyTable.dataSource = dataSource;
    customElements.define('gridy-table', GridyTable);
    dataSource.loadData(data);
</script>
```
## GridyGrid is the container element that simplifies components configuration

GridyGrid is the container element that simplifies components configuration and instantiation. Just wrap your components 
with it and bind DataSource instance or use **gridy-data-source** element.

**gridy-grid** will instantiate and bind dataSource when found **gridy-data-source** subelement. 
Don't forget to start demo server to check outh the code sample below.

```html
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/antd/3.19.8/antd.css" />

<gridy-grid theme="antd" id="gridyGrid" base-path="/node_modules/gridy-grid/src" sort-field="$.title">
    <gridy-data-source fields='[{ "title": "Title", "path": "$.title" },{ "title": "Price", "path": "$.price" }]'
                       datasource-type="DataSourceAjax" url='http://127.0.0.1:8080/unpaged' datapath="$.data"></gridy-data-source>
    <gridy-spinner></gridy-spinner>
    <gridy-table-info id="gridyTableInfo"></gridy-table-info>
    <gridy-filter id="gridyFilter"></gridy-filter>
    <gridy-table id="gridyTable"></gridy-table>
    <gridy-pager id="gridyPager"></gridy-pager>
    <gridy-table-info id="gridyTableInfo"></gridy-table-info>
</gridy-grid>
```

For local data source we may need less args:

```html
<gridy-data-source fields='[{ "title": "Title", "path": "$.title" },{ "title": "Price", "path": "$.price" }]'
                   dataref="gridData"></gridy-data-source>
```

It's also possible to configure data-source with markup instead of attribute:

```html
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/antd/3.19.8/antd.css">

<gridy-grid theme="antd" id="gridyGrid" base-path="/node_modules/gridy-grid/src">
    <gridy-data-source dataref="gridData">
        <gridy-data-field title="Title" path="$.title"></gridy-data-field>
        <gridy-data-field title="Price" path="$.price"></gridy-data-field>
    </gridy-data-source>
    <gridy-spinner></gridy-spinner>
    <gridy-table id="gridyTable"></gridy-table>
    <gridy-pager id="gridyPager"></gridy-pager>
</gridy-grid>
```
 
You are still free to instanciate and configure dataSource programmatically: 

```html
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/antd/3.19.8/antd.css">
<gridy-grid theme="antd" id="gridyGrid">
    <gridy-filter id="gridyFilter"></gridy-filter>
    <gridy-table id="gridyTable"></gridy-table>
    <gridy-pager id="gridyPager"></gridy-pager>
</gridy-grid>

<style>
    th.sort.sort-desc::after {
        content: '▼';
    }
    th.sort.sort-asc::after {
        content: '▲';
    }
</style>
<script type="module">
    import { GridyGrid } from '/node_modules/gridy-grid/src/gridy-grid.js';

    import { DataSourceAjax } from '/node_modules/gridy-grid/src/datasource/data-source-ajax.js';

    let dataSource = new DataSourceAjax();
    dataSource.fields = [
        { title: 'Title', path: '$.title' },
        { title: 'Price', path: '$.price', fmt: (field, value, row) => `<b>$ ${value}</b>`, html: true  }
    ];
    dataSource.url = 'http://127.0.0.1:8080/paged';

    let grid = document.querySelector('#gridyGrid');
    grid.dataSource = dataSource;

    customElements.define('gridy-grid', GridyGrid);

</script>
```

## Separate declarative elements configuration

Without gridy-grid container you'll have to provider every element with configurations and link with the following attributes: 

**dsref** - to specify DataSource element id/global name

**pgref** - to link table to pager

```html
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/antd/3.19.8/antd.css">

<gridy-data-source dataref="gridData" id="dataSource">
    <gridy-data-field title="Title" path="$.title"></gridy-data-field>
    <gridy-data-field title="Price" path="$.price"></gridy-data-field>
</gridy-data-source>
<gridy-table theme="antd" id="gridyGrid" base-path="/node_modules/gridy-grid/src" id="gridyTable"
             dsref="dataSource" pgref="gridyPager" sort-field="$.title"></gridy-table>
<gridy-pager theme="antd" base-path="/node_modules/gridy-grid/src" id="gridyPager" dsref="dataSource"></gridy-pager>
<script type="module">
    import { GridyTable } from '/node_modules/gridy-grid/src/table/gridy-table.js';
    let data = [];
    for (let i = 0; i < 25; i++) {
        data.push({ title: 'row' + i, price: 100 * i })
    }
    window.gridData = data;
    customElements.define('gridy-table', GridyTable);
</script>

```

## Configuration with external element host

Gridy elements can load common configuration attributes from special elements in DOM so you don't need to repeat them for
each element, currently **gridy-config** and **sk-config** (from skinny-widgets) are supported. You can alternate config element 
lookup selector with **configSl** attribute.

```html
<gridy-config
        theme="antd"
        base-path="/node_modules/gridy-grid/src"
        lang="ru_RU"
        id="myConfig"
></gridy-config>

``` 

## Build grid from markup

You can generate some usual table markup with data by yourself and init Gridy Grid for it. 

```html
<gridy-table id="gridyTable" pgref="gridyPager" base-path="/node_modules/gridy-grid/src" theme="antd">
    <table>
        <tbody class="ant-table-tbody">
            <tr><td>row hand 1</td><td><b>$ 0</b></td></tr>
            <tr><td>row hand 2</td><td><b>$ 100</b></td></tr>
            <tr><td>row hand 3</td><td><b>$ 100</b></td></tr>
            <tr><td>row hand 4</td><td><b>$ 100</b></td></tr>
            <tr><td>row hand 5</td><td><b>$ 100</b></td></tr>
        </tbody>
    </table>
</gridy-table>
<gridy-pager id="gridyPager" dsref="gridyTable.dataSource" base-path="/node_modules/gridy-grid/src" theme="antd"></gridy-pager>

<script type="module">
    import { GridyGrid } from '/node_modules/gridy-grid/src/gridy-grid.js';
    customElements.define('gridy-table', GridyTable);
</script>
``` 

## Define row click callback

You can either bind it with general event api using gridy-grid element or gridy-table and rely to path attribue values or
use special property onrowclick or onhrowclick (for heading) for convenience:

```javascript
    gridyTable.onrowclick = function(event) {
        console.log('rowclick', event, this);
    };
```

## Define attributes for cell td element

You can specify any own td's attribute as value or lambda with attr options subset, e.g. you want to implement
cell hover hint

```javascript
    dataSource.fields = [
        { title: 'Title', path: '$.title', attr: { title: (field, value) => `${field.title}` }},
        { title: 'Price', path: '$.price' }
    ];
```

rowattr can be also specified in the same way for tr element, beware multiple same attrs will overwrite each other


## Subscribe to lifecycle events

Gridy grid components are emitting events that can help you to hook the process, e.g. you want to do some stuff after
table header is rendered. All you have to do is to add event listener to particular event.

```javascript
    gridyTable.addEventListener('headersRendered', function(event) {
        console.log('headersRendered', event, this);
    });
```

List of events by elements:

gridy-table:
    headerRowRendered
    headersRendered
    bodyRowRendered
    bodyRendered
    
gridy-table-info:
    tableInfoRendered

gridy-pager:
    pageChanged
    pagerButtonRendered
    pagerRendered

gridy-filter:
    filterChanged
    
    
## Override and Cache Templates

You can place contents inside template tag with matching ids to make renderer overriding templates with by your versions.
The highest priority is for lookup inside current html element, then document is searched and after that template path
attribute is loaded.

So you can copy markup from theme inside tag, modify and component will use it. Here is the map of template attributes to 
 loaded element ids below.

gridy-table:
    head-tpl-path -> tableHeadTpl
    body-tpl-path -> tableBodyTpl
    
gridy-table-info:
    tpl-path -> tableInfoTpl
    noent-tpl-path -> noEntriesInfoTpl (optional)
        
gridy-pager:
    body-tpl-path -> pagerBodyTpl
    item-active-tpl-path -> pagerActiveItemTpl
    item-tpl-path -> pagerItemTpl
    
gridy-filter:
    tpl-path -> pagerItemTpl
        
gridy-spinner:
    tpl-path -> spinnerTpl
      

### Loading templates from bundles

http/2+ allows to do request multiplexing that means a lot of smaller requests to the same host are better than big blocking 
requests to multiple hosts. But if you think you still need to load all in one bundle, you can use aggregated template bundles
loaded with js or included to page by server.

```html
<script>
    const loadTemplates = async () => {
        await fetch('node_modules/gridy-grid/dist/gridy-antd.tpls.native.html')
            .then(response => response.text())
            .then(text => document.body.insertAdjacentHTML('beforeend', text));
    };
    loadTemplates();
</script>
``` 


### Tweak renderer behaviour

You can control template loading/caching behaviour either individually or with **gridy-grid** element attributes:

**rd-cache** - enables/disables templates caching and inline overriding (default: enabled)

**rd-cache-global** - if cached template wasn't found allows to lookup in document (default: enabled)

You can even enable rendering with Handlebars templating library for your overriden templates. 
To have this you must provide Handlebars static import.

```html
<script src="./node_modules/handlebars/dist/handlebars.js"></script>
```

or any other way to set global Handlebars instance to window or inject renderer property (not attribute).

Also add option **rd-var-render** enabled for renderer instance passed to **gridy-grid** or **gridy-table** element as attribute.
It can be 'handlebars' to load handlebars or loader window function name to call. 
If attribute is not defined, simplified built-in renderer that support only variables will be used.

There is an option to use handlebars templates format instead of Native Templates to support legacy browsers like
Internet Explorer. It can be enabled with **rd-tpl-fmt="handlebars"** attribute to **gridy-grid** element. You 
will also need a kinda transpiler with modules, classes etc. support enabled to be able to see rendering results.
In addition handlebars tpl format requires jquery library present in runtime.

## Ajax Data Source

Data is loaded from REST service, ant.design framework theming enabled. Paging and filtering components added;

```html
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/antd/3.19.8/antd.css">

<gridy-filter id="gridyFilter"></gridy-filter>
<gridy-table id="gridyTable"></gridy-table>
<gridy-pager id="gridyPager"></gridy-pager>

<style>
    th.sort.sort-desc::after {
        content: '▼';
    }
    th.sort.sort-asc::after {
        content: '▲';
    }
</style>
<script type="module">
    import { GridyTable } from '/node_modules/gridy-grid/src/table/gridy-table.js';
    import { GridyPager } from '/node_modules/gridy-grid/src/pager/gridy-pager.js';
    import { GridyFilter } from '/node_modules/gridy-grid/src/filter/gridy-filter.js';
    import { DataSourceLocal } from '/node_modules/gridy-grid/src/datasource/data-source-local.js';
    import { DataSourceAjax } from '/node_modules/gridy-grid/src/datasource/data-source-ajax.js';

    //let dataSource = new DataSourceLocal();
    let dataSource = new DataSourceAjax();
    dataSource.fields = [
        { title: 'Title', path: '$.title' },
        { title: 'Price', path: '$.price' }
    ];
    dataSource.url = 'http://127.0.0.1:8080/paged';

    let filter = document.getElementById('gridyFilter');
    filter.tplPath = '/node_modules/gridy-grid/src/theme/antd/filter.tpl.html';
    filter.dataSource = dataSource;

    let pager = document.getElementById('gridyPager');
    pager.dataSource = dataSource;
    pager.theme = 'antd';
    pager.bodyTplPath = '/node_modules/gridy-grid/src/theme/antd/pager-body.tpl.html';
    pager.itemTplPath = '/node_modules/gridy-grid/src/theme/antd/pager-item.tpl.html';
    pager.itemActiveTplPath = '/node_modules/gridy-grid/src/theme/antd/pager-item-active.tpl.html';

    let table = document.getElementById('gridyTable');
    table.dataSource = dataSource;
    table.pager = pager;
    table.filter = filter;

    table.headers = true;
    table.theme = 'antd';
    table.headTplPath = '/node_modules/gridy-grid/src/theme/antd/table-head.tpl.html';
    table.bodyTplPath = '/node_modules/gridy-grid/src/theme/antd/table-body.tpl.html';

    customElements.define('gridy-filter', GridyFilter);
    customElements.define('gridy-table', GridyTable);
    customElements.define('gridy-pager', GridyPager);

</script>
```

## Internationalization

gridy-grid element supports lang attribued that allows developer to provide elements translated by locale resources.
Resources for translation are shipped with themes.

```html
    <gridy-grid theme="antd" id="gridyGrid" lang="ru_RU">
        <gridy-filter id="gridyFilter"></gridy-filter>
        <gridy-table id="gridyTable"></gridy-table>
        <gridy-pager id="gridyPager"></gridy-pager>
    </gridy-grid>
```

## Use with compatibility bundle (old browser support)

To have browsers up to IE11 supported you definitely need to load WebComponents polyfills.

Rendering engine must be changed to handlebars.

Also jquery needs to be loaded before bundle as it is used to generate table markup.

Initializing code must be wrapped into WebComponentsReady hook event.

In the example below polyfills from skinny-widgets library are used. You should install it or plug your own.

```shell script
npm i skinny-widgets --save
```

```html
<script src="/node_modules/skinny-widgets/compat/dialog-polyfill.js"></script>
<script src="/node_modules/skinny-widgets/compat/fetch.umd.js"></script>
<script src="/node_modules/skinny-widgets/compat/polyfill.min.js"></script>
<script src="/node_modules/skinny-widgets/compat/webcomponents-lite.js"></script>
<script src="/node_modules/skinny-widgets/compat/custom-elements-es5-adapter.js"></script>
<script src="/node_modules/skinny-widgets/compat/event-target.js"></script>

<link rel="stylesheet" type="text/css" href="/node_modules/gridy-grid/antd.min.css">

<sk-config
        theme="antd"
        base-path="/node_modules/gridy-grid/src"
        lang="ru"
        id="gridConfig"
></sk-config>

<gridy-table config-sl="#gridConfig" id="grid" pgref="pager" rd-tpl-fmt="handlebars"></gridy-table>
<gridy-pager config-sl="#gridConfig" id="pager" dsref="dataSource"></gridy-pager>

<script src="/node_modules/skinny-widgets/dist/skinny-widgets-bundle.js"></script>
<script src="/node_modules/jquery/dist/jquery.js"></script>
<script src="/node_modules/gridy-grid/dist/gridy-grid-bundle.js"></script>
<script>
    window.addEventListener('WebComponentsReady', function(e) {
        var dataSource = new DataSourceLocal();
        dataSource.fields = [
            { title: 'Title', path: '$.title' },
            { title: 'Price', path: '$.price' }
        ];
        var data = [];
        for (let i = 0; i < 25; i++) {
            data.push({ title: 'row' + i, price: 100 * i })
        }
        grid.dataSource = dataSource;
        
        customElements.define('sk-config', SkConfig);
        customElements.define('gridy-pager', GridyPager);
        customElements.define('gridy-table', GridyTable);
        dataSource.loadData(data);
    });
</script>
```

## Development

You can run sample data backend and index.html serving from this project's root.

```shell script
npm run server
```


## Use with SystemJS module loader 

Gridy Grid currently has only one external runtime dependency that is actually source bundled to avoid problems 
in untranspilled environments. So in case you want to use that library (complets) from it's own npm package or somewhere else 
e.g. shared bundle you may use SystemJS loader map to resolve Gridy's internal dependencies.
 
 First add and setup module loader for your project.

``
npm i systemjs systemjs-transform-babel --save
``

And do configuration that will transpile and resolve the code.

```html
<script src="/node_modules/systemjs/dist/system.js"></script>
<script src="/node_modules/systemjs/dist/extras/transform.js"></script>
<script src="/node_modules/systemjs-transform-babel/dist/babel-transform.js"></script>
<script type="systemjs-importmap">
{
  "imports": {
    "gridy-grid": "./node_modules/gridy-grid/src/gridy-grid.js",
    "data-source-local": "./node_modules/gridy-grid/src/datasource/data-source-local.js",
    "/node_modules/gridy-grid/complets/": "/node_modules/complets/"
  }
}
</script>
<script>
    Promise.all([System.import('gridy-grid'), System.import('data-source-local')]).then((m) => {
        let dataSource = new m[1].DataSourceLocal();
        //let dataSource = new DataSourceAjax();
        dataSource.fields = [
            { title: 'Title', path: '$.title' },
            { title: 'Price', path: '$.price' }
        ];
        let grid = document.querySelector('#gridyGrid');
        grid.dataSource = dataSource;

        customElements.define('gridy-grid', m[0].GridyGrid);

        // generate sample data for DataSourceLocal
        let data = [];
        for (let i = 0; i < 25; i++) {
            data.push({ title: 'row' + i, price: 100 * i })
        }
        dataSource.loadData(data);
    });
</script>
```

### Update complets library version

```shell script
git submodule update --remote --merge
```

## Running tests

open test/tests.html from browser with statically served project root

or install webcomponents test runner

```shell script
sudo npm install --global web-component-tester --unsafe-perm
```

and run with

```shell script
wct --npm
```