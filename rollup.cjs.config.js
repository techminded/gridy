
import babel from 'rollup-plugin-babel';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonJS from 'rollup-plugin-commonjs'

export default {
  input: 'index.js',
  output: {
    globals: {
    },
    file: 'dist/gridy-grid-bundle.cjs.js',
    format: 'cjs'
  },
  external: [  ],
  plugins: [
    babel({ runtimeHelpers: true }),
    nodeResolve({ mainFields: ['jsnext:main']}),
    commonJS({})
  ]
};
