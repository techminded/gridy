
import { Renderer } from '../complets/renderer.js';

import { GridyFilter } from './filter/gridy-filter.js';
import { GridyPager } from './pager/gridy-pager.js';
import { GridyTable } from './table/gridy-table.js';
import { GridyTableInfo } from './table/gridy-table-info.js';
import { GridySpinner } from './gridy-spinner.js';
import { DataSourceLocal } from './datasource/data-source-local.js';
import { DataSourceAjax } from './datasource/data-source-ajax.js';
import { GridyDataSource } from './datasource/gridy-data-source.js';
import { GridyDataField } from './datasource/gridy-data-field.js';
import { GridyConfig } from './gridy-config.js';

export class GridyGrid extends HTMLElement {

    // filter: GridyFilter;
    // pager: GridyPager;
    // table: GridyTable;
    // info: GridyTableInfo;

    get theme() {
        return this.getAttribute('theme') || 'default';
    }

    set dataSource(dataSource) {
        this._dataSource = dataSource;
    }

    get dataSource() {
        return this._dataSource;
    }

    get lang() {
        return this.getAttribute('lang') || null;
    }

    get sortField() {
        return this.getAttribute('sort-field');
    }

    set sortField(sortField) {
        return this.setAttribute('sort-field', sortField);
    }

    get sortDirection() {
        return this.getAttribute('sort-direction');
    }

    set sortDirection(sortDirection) {
        return this.setAttribute('sort-direction', sortDirection);
    }

    get configSl() {
        return this.getAttribute('configSl') || 'sk-config, gridy-config';
    }

    get configEl() {
        if (! this._configEl) {
            this._configEl = document.querySelector(this.configSl);
        }
        return this._configEl;
    }

    get configTn() {
        return this.getAttribute('config-tn') || 'gridy-config';
    }

    set configTn(configTn) {
        return this.setAttribute('config-tn', configTn);
    }

    get tableTn() {
        return this.getAttribute('table-tn') || 'gridy-table';
    }

    set tableTn(tableTn) {
        return this.setAttribute('table-tn', tableTn);
    }

    get dsTn() {
        return this.getAttribute('ds-tn') || 'gridy-data-source';
    }

    set dsTn(dsTn) {
        return this.setAttribute('gridy-data-source', dsTn);
    }

    get dfTn() {
        return this.getAttribute('df-tn') || 'gridy-data-field';
    }

    set dfTn(dfTn) {
        return this.setAttribute('gridy-data-field', dfTn);
    }

    get filterTn() {
        return this.getAttribute('filter-tn') || 'gridy-filter';
    }

    set filterTn(filterTn) {
        return this.setAttribute('gridy-filter', filterTn);
    }

    get pagerTn() {
        return this.getAttribute('pager-tn') || 'gridy-pager';
    }

    set pagerTn(pagerTn) {
        return this.setAttribute('gridy-pager', pagerTn);
    }

    get infoTn() {
        return this.getAttribute('info-tn') || 'gridy-table-info';
    }

    set infoTn(infoTn) {
        return this.setAttribute('gridy-table-info', infoTn);
    }

    get spinnerTn() {
        return this.getAttribute('spinner-tn') || 'gridy-spinner';
    }

    set spinnerTn(spinnerTn) {
        return this.setAttribute('gridy-spinner', spinnerTn);
    }


    get basePath() {
        return this.getAttribute('base-path') || this.getAttribute('basePath')
            || this.getAttribute('basepath') || './src';
    }

    useConfigEl() {
        if (! customElements.get(this.configTn)) {
            customElements.define(this.configTn, GridyConfig);
        }
        if (this.configEl) {
            this.configEl.configureElement(this);
        }
    }

    forwardProp(propName, target) {
        if (this[propName]) {
            target[propName] = this[propName];
        }
    }

    forwardProps(props, target) {
        for (let prop of props) {
            this.forwardProp(prop, target);
        }
    }

    get supportedElements() {
        let elements = {};
        elements[this.dsTn] = GridyDataSource;
        elements[this.dfTn] = GridyDataField;
        elements[this.configTn] = GridyConfig;
        elements[this.tableTn] = GridyTable;
        elements[this.filterTn] = GridyFilter;
        elements[this.pagerTn] = GridyPager;
        elements[this.infoTn] = GridyTableInfo;
        elements[this.spinnerTn] = GridySpinner;
        return elements;
    }

    regCustomEl(el) {
        this.regCustomElOrRender(el);
    }

    regCustomEls(els) {
        for (let el of els) {
            this.regCustomEl(el);
        }
    }
    regCustomElOrRender(name, el) {
        if (!customElements.get(name)) {
            customElements.define(name, this.supportedElements[name]);
        } else {
            if (el) {
                el.connectedCallback();
            }
        }
    }

    connectedCallback() {
        this.useConfigEl();
        Renderer.configureForElement(this);

        if (!this.dataSource) {
            let dataSourceEl = this.querySelector(this.dsTn);
            if (dataSourceEl != null) {
                this.regCustomEl(this.dsTn);
                dataSourceEl.config();
                this.dataSourceEl = dataSourceEl;
                this.dataSource = dataSourceEl.dataSource;
            }
        }

        this.filters = [];
        let filters = document.querySelectorAll(this.filterTn);
        for (let filter of filters) {
            this.forwardProps(['dataSource', 'renderer', 'basePath', 'theme'], filter);
            filter.tplPath = this.basePath + '/theme/' + this.theme + '/filter.tpl.html';
            this.filters.push(filter);
        }

        let pager = this.querySelector(this.pagerTn);
        if (pager !== null) {
            this.forwardProps(['dataSource', 'renderer', 'basePath', 'theme'], pager);
            if (!pager.bodyTplPath) {
                pager.bodyTplPath = pager.basePath + '/theme/' + pager.theme + '/pager-body.tpl.html';
            }
            if (!pager.itemTplPath) {
                pager.itemTplPath = pager.basePath + '/theme/' + pager.theme + '/pager-item.tpl.html';
            }
            if (!pager.itemActiveTplPath) {
                pager.itemActiveTplPath = pager.basePath + '/theme/' + pager.theme + '/pager-item-active.tpl.html';
            }
            this.pager = pager;
            this.regCustomElOrRender(this.pagerTn, this.pager);
        }

        let table = this.querySelector(this.tableTn);
        if (table !== null) {
            this.forwardProps(['dataSource', 'renderer', 'basePath', 'theme', 'pager', 'filters', 'onrowclick', 'onhrowclick'], table);
            table.headers = true;
            if (!table.sortField && this.sortField) {
                table.sortField = this.sortField;
                table.setAttribute('sort-field', this.sortField);
            }
            if (!table.sortDirection && this.sortDirection) {
                table.sortDirection = this.sortDirection;
                table.setAttribute('sort-direction', this.sortDirection);
            }
            if (!table.headTplPath && !table.getAttribute('head-tpl-path')) {
                table.headTplPath = this.basePath + '/theme/' + table.theme + '/table-head.tpl.html';
            }
            if (!table.bodyTplPath && !table.getAttribute('body-tpl-path')) {
                table.bodyTplPath = this.basePath + '/theme/' + table.theme + '/table-body.tpl.html';
            }
            this.table = table;
            this.regCustomElOrRender(this.tableTn, this.table);
        } else {
            console.warn('table element not found in gridy-grid container');
        }

        this.infos = [];
        let infos = this.querySelectorAll(this.infoTn);
        for (let info of infos) {
            if (info != null) {
                this.forwardProps(['dataSource', 'renderer', 'basePath', 'theme', 'pager', 'lang'], info);
                if (!info.tplPath) {
                    info.tplPath = this.basePath + '/theme/' + info.theme + '/table-info.tpl.html';
                }
                this.infos.push(info);
                this.regCustomElOrRender(this.infoTn, info);
            }
        }

        let spinner = this.querySelector(this.spinnerTn);
        if (spinner !== null) {
            this.forwardProps(['renderer', 'basePath', 'theme'], spinner);
            spinner.tplPath = spinner.basePath + '/theme/' + spinner.theme + '/spinner.tpl.html';
            this.spinner = spinner;
            this.regCustomElOrRender(this.spinnerTn, this.spinner);
            if (this.dataSource) {
                if (this.dataSource instanceof DataSourceAjax) {
                    this.dataSource.addEventListener('dataFetchStarted', (event) => {
                        spinner.show();
                    });
                } else {
                    this.dataSource.addEventListener('dataLoadStarted', (event) => {
                        spinner.show();
                    });
                }
                this.dataSource.addEventListener('dataLoaded', (event) => {
                    spinner.hide();
                });
            }
        }

        this.regCustomEls([this.filterTn, this.pagerTn, this.tableTn, this.infoTn, this.dfTn]);

        if (this.dataSourceEl
            && this.dataSourceEl.getAttribute('datasource-type') === 'DataSourceLocal') {
            this.dataSourceEl.loadDataRef();
        }

    }
}