
import { Renderer } from '../../complets/renderer.js';
import { DataSourceLocal } from './data-source-local.js';
import { DataSourceAjax } from './data-source-ajax.js';


export class GridyDataSource extends HTMLElement {

    get fields() {
        return this.getAttribute('fields');
    }

    config() {
        let dataSourceType = this.getAttribute('data-source-type');
        if (!dataSourceType) {
            this.dataSource = new DataSourceLocal();
        } else {
            if (dataSourceType === 'DataSourceAjax') {
                this.dataSource = new DataSourceAjax();
            } else {
                this.dataSource = new DataSourceLocal();
            }
        }
        if (this.getAttribute('fields')) {
            this.dataSource.fields = JSON.parse(this.getAttribute('fields'));
        } else {
            let fieldEls = this.querySelectorAll('gridy-data-field');
            let fields = [];
            for (let fieldEl of fieldEls) {
                let field = {};
                field.title = fieldEl.getAttribute('title');
                field.path = fieldEl.getAttribute('path');
                let fmt = fieldEl.getAttribute('fmt');
                if (fmt) {
                    field.fmt = fmt;
                }
                let html = fieldEl.getAttribute('html');
                if (html) {
                    field.html = html;
                }
                fields.push(field);
            }
            this.dataSource.fields = fields;
        }
        this.setAttribute('datasource-type', this.dataSource.constructor.name);
    }

    loadDataRef() {
        let dataref = this.getAttribute('dataref');
        if (typeof window[dataref] === 'function') {
            window[dataref].call(this);
        } else if (dataref && window[dataref]) {
            this.dataSource.loadData(window[dataref]);
        }
    }

    connectedCallback() {
        if (!this.renderer) {
            this.renderer = new Renderer();
        }
    }

}