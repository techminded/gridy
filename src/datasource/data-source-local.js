
import '../../complets/jsonpath.js';

let JSONPath;
if (typeof jsonpath.JSONPath === 'function') {
    JSONPath = new jsonpath.JSONPath()
} else {
    JSONPath = new jsonpath.JSONPath.default.JSONPath();
}

export class DataSourceLocal extends EventTarget {

    //data: [];
    //loadTimest: number;
    //dataPath: string; // jsonpath for data rows root;

    constructor() {
        super();
        this.loadTimest = -1;
    }

    init() {

    }

    loadData(data, params) {
        this.dispatchEvent(new CustomEvent('dataLoadStarted', {
            detail: params
        }));
        params = params || {};
        let dataRows = this.dataPath ? JSONPath.query(data, this.dataPath)[0] : data;
        this.data = params.data = dataRows;
        this.loadTimest = Date.now();
        this.dispatchEvent(new CustomEvent('dataLoaded', {
            detail: params
        }));
    }

    get total() {
        return this.dataFiltered ? this.dataFiltered.length : this.data.length;
    }

    get data() {
        return this.dataFiltered || this._data || [];
    }

    set data(data) {
        this._data = data;
    }

    sort(field, direction) {
        let sortFunc = (a, b) => {
            let aVal = null;
            let bVal = null;
            if (field && field.startsWith('$')) {
                aVal = JSONPath.query(a, field)[0];
                bVal = JSONPath.query(b, field)[0];
            } else {
                aVal = a[field];
                bVal = b[field];
            }
            if (aVal === bVal) {
                return 0;
            }
            if (direction === undefined || direction === 'asc') {
                if (typeof aVal === 'string') {
                    return aVal.localeCompare(bVal);
                } else {
                    return aVal - bVal;
                }
            } else {
                if (typeof aVal === 'string') {
                    return bVal.localeCompare(aVal);
                } else {
                    return bVal - aVal;
                }
            }

        };
        this.data.sort(sortFunc);
        if (this.dataFiltered) {
            this.dataFiltered.sort(sortFunc);
        }
    }

    filter(value) {
        this.dataFiltered = this._data.filter((dataItem) => {
            for (let field of this.fields) {
                if (typeof field == 'string') {
                    if (Array.isArray(value)) {
                        for (let val of value) {
                            if (dataItem[field] && (dataItem[field]).toString().indexOf(val) >= 0) {
                                return true;
                            }
                        }
                    } else {
                        if (dataItem[field] && (dataItem[field]).toString().indexOf(value) >= 0) {
                            return true;
                        }
                    }
                } else {
                    let result = JSONPath.query(dataItem, field.path)[0];
                    if (result !== null) {
                        let itemValue = (result).toString();
                        if (Array.isArray(value)) {
                            for (let val of value) {
                                if (itemValue && itemValue.indexOf(val) >= 0) {
                                    return true;
                                }
                            }
                        } else {
                            if (itemValue && itemValue.indexOf(value) >= 0) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        })
    }
}