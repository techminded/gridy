
import { DataSourceLocal } from './data-source-local.js';

import { XmlHttpClient } from '../../complets/http/xml-http-client.js';

export class DataSourceAjax extends DataSourceLocal {

    //url: string;
    //method: string;

    //backPaged: boolean;
    //loadOnInit: boolean;

    //http;
    //auth;

    //data: [];

    constructor() {
        super();
        this.loadOnInit = true;
        this.loadDate = -1;
    }

    init() {
        this.http = this.http ? this.http : new XmlHttpClient();
        if (this.auth) {
            this.http.auth = this.auth;
        }
        if (this.loadOnInit) {
            this.fetchData();
        }
    }

    buildUrl(params) {
        let page = params.page || 1;
        let perPage = params.perPage || 10;
        if (params.backPaged) {
            let url = this.url + '?page=' + page + '&perPage=' + perPage;
            if (params.sortField) {
                url += '&sortField=' + params.sortField + '&sortDirection=' + params.sortDirection;
            }
            return url;
        } else {
            return this.url;
        }
    }

    fetchData(params) {
        params = params || {};
        return new Promise((resolve, reject) => {
            if (! this.url) {
                reject("No data url provided");
            }
            this.dispatchEvent(new CustomEvent('dataFetchStarted', {
                detail: params
            }));
            this.http.get(this.buildUrl(params)).then((request) => {
                this.dispatchEvent(new CustomEvent('dataFetched', {
                    detail: params
                }));
                let response = request.response;
                let detail = {
                    data: this.data,
                    backPaged: params.backPaged,
                    totalResults: response.totalResults || this.data.length
                };
                if (params.sortField || response.sortField) {
                    detail.sortField = response.sortField || params.sortField;
                    detail.sortDirection = response.sortDirection || params.sortDirection;
                }
                this.data = response;
                this.loadData(this.data, detail);
                resolve(this.data);
            });
        });
    }


}