
import { Renderer } from '../../complets/renderer.js';
import { GridyConfig } from '../gridy-config.js';

const PER_PAGE_DEFAULT = 10;

export class GridyPager extends HTMLElement {

    //headers: boolean;

    //renderer: Renderer;
    //theme: string;

    //tpl;

    //curPageNum: number;
    //totalPages: number;
    //perPage: number;
    //totalResults: number;

    //bodyTplPath;
    //bodyTpl;

    //itemActiveTplPath;
    //itemActiveTpl;

    //itemTplPath;
    //itemTpl;


    get bodyTplPath() {
        return this.getAttribute('body-tpl-path') || this.basePath + '/theme/' + this.theme + '/pager-body.tpl.html';
    }

    set bodyTplPath(tplPath) {
        this.setAttribute('body-tpl-path', tplPath);
    }

    get itemTplPath() {
        return this.getAttribute('item-tpl-path') || this.basePath + '/theme/' + this.theme + '/pager-item.tpl.html';
    }

    set itemTplPath(tplPath) {
        this.setAttribute('item-tpl-path', tplPath);
    }

    get itemActiveTplPath() {
        return this.getAttribute('item-active-tpl-path') || this.basePath + '/theme/' + this.theme + '/pager-item-active.tpl.html';
    }

    set itemActiveTplPath(tplPath) {
        this.setAttribute('item-active-tpl-path', tplPath);
    }

    get theme() {
        return this.getAttribute('theme');
    }

    set theme(theme) {
        return this.setAttribute('theme', theme);
    }

    get basePath() {
        return this.getAttribute('base-path') || this.getAttribute('basepath') || null;
    }

    set basePath(basePath) {
        this.setAttribute('base-path', basePath);
    }

    get configSl() {
        return this.getAttribute('configSl') || 'sk-config, gridy-config';
    }

    get configEl() {
        if (! this._configEl) {
            this._configEl = document.querySelector(this.configSl);
        }
        return this._configEl;
    }

    onclick(event) {
        // get current or parent data-num attr value
        let numAttr = event.target.dataset.num || event.target.parentElement.dataset && event.target.parentElement.dataset.num;
        if (! numAttr) {
            event.preventDefault();
            event.stopPropagation();
            return null;
        }
        let backPaged = event.target.dataset.backPaged || event.target.parentElement.dataset.backpaged;
        let detail = {
            page: this.curPageNum, backPaged: backPaged
        };
        let sortField = event.target.dataset.sortfield || event.target.parentElement.dataset.sortfield;
        let sort = undefined;
        if (sortField) {
            sort = {};
            let sortDirection = event.target.dataset.sortdirection || event.target.parentElement.dataset.sortdirection;
            sort.sortField = detail.sortField = sortField;
            sort.sortDirection = detail.sortDirection = sortDirection;
        }
        let num = parseInt(numAttr);
        if (num !== 0) {
            this.curPageNum = detail.page = num;
            this.dispatchEvent(new CustomEvent('pageChanged',
    { detail: detail }
            ));
            if (! backPaged) {
                this.renderButtons(backPaged, sort);
            }
        }
    }

    async renderPageButton(num, backPaged, sort) {
        let context = Object.assign({ num }, this);

        let templateEl = num === this.curPageNum ? this.itemActiveTpl : this.itemTpl;
        let wrap = this.renderer.prepareTemplate(templateEl);

        let button = (wrap.tagName === 'LI') ? wrap : wrap.querySelector('li');
        button.setAttribute('data-num', num);
        if (backPaged) {
            button.setAttribute('data-backPaged', 1);
        }
        if (sort) {
            button.setAttribute('data-sortField', sort.sortField);
            button.setAttribute('data-sortDirection', sort.sortDirection || 'asc');
        }
        let rendered = this.renderer.renderMustacheVars(button, context);
        this.querySelector('ul').insertAdjacentHTML('beforeend', rendered);
        this.dispatchEvent(new CustomEvent('pagerButtonRendered'));
    }

    renderButtons(backPaged, sort) {
        let context = this;
        Promise.all([
            this.renderer.mountTemplate(this.bodyTplPath, 'pagerBodyTpl', this),
            this.renderer.mountTemplate(this.itemTplPath, 'pagerItemTpl', this),
            this.renderer.mountTemplate(this.itemActiveTplPath, 'pagerActiveItemTpl', this)
        ]).then((templateEls) => {
            this.bodyTpl = templateEls[0];
            this.itemTpl = templateEls[1];
            this.itemActiveTpl = templateEls[2];
            this.bodyEl = this.renderer.prepareTemplate(this.bodyTpl);
            this.innerHTML = '';
            this.appendChild(this.bodyEl);

            if (this.totalPages >= 5) {
                if (this.curPageNum > 2) {
                    if (this.curPageNum + 2 < this.totalPages) { // middle pages
                        for (let i = this.curPageNum - 2; i <= this.curPageNum + 2; i++) {
                            this.renderPageButton(i, backPaged, sort);
                        }
                    } else { // last pages
                        for (let i = this.curPageNum - 2; i <= this.totalPages; i++) {
                            this.renderPageButton(i, backPaged, sort);
                        }
                    }
                } else { // start pages, big resultset
                    for (let i = 1; i <= 5; i++) {
                        this.renderPageButton(i, backPaged, sort);
                    }
                }

            } else { //start pages, small resultset
                for (let i = 1; i <= this.totalPages; i++) {
                    this.renderPageButton(i, backPaged, sort);
                }
            }
            this.dispatchEvent(new CustomEvent('pagerRendered'));
        });
    }

    initPageButtons(data, backPaged, sort) {
        if (! this.totalResults && this.totalPages) {
            if (this.dataSource) {
                this.totalPages = Math.ceil(this.dataSource.total / this.perPage);
            } else {
                this.totalPages = Math.ceil(data.length / this.perPage);
            }
        }
        if (this.totalPages > 1) {
            this.renderButtons(backPaged, sort);
        }
    }

    get perPage() {
        return parseInt(this.getAttribute('per-page')) || PER_PAGE_DEFAULT;
    }

    set perPage(perPage) {
        this.setAttribute('per-page', perPage);
    }

    get curPageNum() {
        return parseInt(this.getAttribute('cur-page-num')) || 1;
    }

    set curPageNum(curPageNum) {
        this.setAttribute('cur-page-num', curPageNum);
    }

    get totalResults() {
        return parseInt(this.getAttribute('total-results')) || 0;
    }

    set totalResults(totalResults) {
        this.setAttribute('total-results', totalResults);
    }

    get totalPages() {
        return parseInt(this.getAttribute('total-pages')) || 1;
    }

    set totalPages(totalPages) {
        this.setAttribute('total-pages', totalPages);
    }

    get dsRef() {
        return this.getAttribute('dsref');
    }

    bindDataSource(dataSource) {
        if (dataSource) {
            this.dataSource = dataSource;
        }
        this.dataSource.addEventListener('dataLoaded', (event) => {
            if (event.detail.totalResults) {
                this.totalResults = event.detail.totalResults;
                this.totalPages = Math.ceil(this.totalResults / this.perPage);
            }
            let data = event.detail.data;
            let backPaged = event && event.detail.backPaged || undefined;
            let sort = { sortField: event.detail.sortField, sortDirection: event.detail.sortDirection };
            this.initPageButtons(data, backPaged, sort);
        });
    }

    useConfigEl() {
        if (! customElements.get('gridy-config')) {
            customElements.define('gridy-config', GridyConfig);
        }
        if (this.configEl) {
            this.configEl.configureElement(this);
        }
    }

    connectedCallback() {
        this.curPageNum = 1;

        this.useConfigEl();
        Renderer.configureForElement(this);

        if (! this.dataSource) {
            if (this.dsRef && window[this.dsRef]) {
                if (typeof window[this.dsRef] === 'function') {
                    window[this.dsRef].call(this);
                } else {
                    let dsEl = window[this.dsRef];
                    this.dataSource = dsEl.dataSource;
                }
            } else {
                if (this.dsRef && typeof this.dsRef === 'string' && this.dsRef.indexOf('.') > 0) {
                    let pathTokens = this.dsRef.split('.');
                    let root = window[pathTokens[0]];
                    if (root) {
                        let ds = root[pathTokens[1]];
                        if (ds) {
                            this.dataSource = ds;
                        }
                    }
                }
            }
        }
        if (this.dataSource) {
            this.bindDataSource();
            if (this.dataSource.total > 0) {
                this.initPageButtons(this.dataSource.data, false);
            }
        }
        this.addEventListener('click', this.onclick.bind(this));
    }
}