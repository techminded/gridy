
import { Renderer } from '../complets/renderer.js';
import { GridyConfig } from './gridy-config.js';

export class GridySpinner extends HTMLElement {

    get display() {
        return this.getAttribute('display') || 'none';
    }
    set display(display) {
        this.setAttribute('display', display);
        if (display === 'none') {
            this.hide();
        } else {
            this.show();
        }
    }

    show() {
        this.style.display = 'block';
    }

    hide() {
        this.style.display = 'none';
    }

    get tplPath() {
        return this.getAttribute('tpl-path') || '../theme/' + this.theme + '/spinner.tpl.html';
    }

    set tplPath(tplPath) {
        this.setAttribute('tpl-path', tplPath);
    }

    get configSl() {
        return this.getAttribute('configSl') || 'sk-config, gridy-config';
    }

    get configEl() {
        if (! this._configEl) {
            this._configEl = document.querySelector(this.configSl);
        }
        return this._configEl;
    }

    useConfigEl() {
        if (! customElements.get('gridy-config')) {
            customElements.define('gridy-config', GridyConfig);
        }
        if (this.configEl) {
            this.configEl.configureElement(this);
        }
    }

    connectedCallback() {
        this.hide();
        this.basePath = this.getAttribute('basePath') || './src';

        this.useConfigEl();
        Renderer.configureForElement(this);

        let context = this;
        this.renderer.mountTemplate(this.tplPath, 'spinnerTpl', this, context).then((templateEl) => {
            this.tpl = templateEl;
            let el = this.renderer.prepareTemplate(this.tpl);
            this.appendChild(el);
        });
    }
}