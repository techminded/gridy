
import { Renderer } from '../../complets/renderer.js';

import '../../complets/jsonpath.js';

let JSONPath = new jsonpath.JSONPath();

import { GridyDataSource } from './../datasource/gridy-data-source.js';
import { GridyPager } from './../pager/gridy-pager.js';

import { DataSourceLocal } from '../datasource/data-source-local.js';
import { GridyConfig } from '../gridy-config.js';

export class GridyTable extends HTMLElement {

    //headers: boolean;

    //renderer: Renderer;
    //theme: string;

    //pager: GridyPager;

    //tableEl;
    //bodyEl;
    //headTplPath;
    //headTpl;
    //bodyTplPath;
    //bodyTpl;

    //sortField: string;
    //sortDirection: string;

    //renderTimest: number;


    constructor() {
        super();
        this.renderTimest = -1;
    }

    get headers() {
        if (this.getAttribute('headers') === 'false') {
            return false;
        }
        return true;
    }

    set headers(headers) {
        this.setAttribute('headers', headers);
    }

    get sortField() {
        return this.getAttribute('sort-field');
    }

    set sortField(sortField) {
        this.setAttribute('sort-field', sortField);
    }

    get sortDirection() {
        return this.getAttribute('sort-direction');
    }

    set sortDirection(sortDirection) {
        this.setAttribute('sort-direction', sortDirection);
    }

    get tplPath() {
        return this.getAttribute('tpl-path') || this.basePath + '/theme/' + this.theme + '/table.tpl.html';
    }

    set tplPath(tplPath) {
        this.setAttribute('tpl-path', tplPath);
    }

    get headTplPath() {
        return this.getAttribute('head-tpl-path') || this.basePath + '/theme/' + this.theme + '/table-head.tpl.html';
    }

    set headTplPath(tplPath) {
        this.setAttribute('head-tpl-path', tplPath);
    }

    get bodyTplPath() {
        return this.getAttribute('body-tpl-path') || this.basePath + '/theme/' + this.theme + '/table-body.tpl.html';
    }

    set bodyTplPath(tplPath) {
        this.setAttribute('body-tpl-path', tplPath);
    }

    get theme() {
        return this.getAttribute('theme') || 'default';
    }

    set theme(theme) {
        this.setAttribute('theme', theme);
    }

    get basePath() {
        return this.getAttribute('base-path') || this.getAttribute('basePath')
            || this.getAttribute('basepath') || null;
    }

    set basePath(basePath) {
        this.setAttribute('base-path', basePath);
    }

    get configSl() {
        return this.getAttribute('config-sl') || this.getAttribute('configSl') || 'sk-config';
    }

    get configEl() {
        if (! this._configEl) {
            this._configEl = document.querySelector(this.configSl);
        }
        return this._configEl;
    }

    get tableEl() {
        if (! this._tableEl) {
            this._tableEl = this.querySelector('table');
        }
        return this._tableEl;
    }


    changeSort(event) {
        let oldSortField = this.sortField;
        let newSortField = event.target.dataset.sort;
        if (oldSortField === newSortField) {
            this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
        } else {
            this.sortDirection = 'asc';
        }
        this.sortField = newSortField;

        if (this.backPaged) {
            this.dataSource.loadData({
                backPaged: true, page: this.pager.curPageNum,
                sortField: this.sortField, sortDirection: this.sortDirection
            });
        } else {
            this.dataSource.sort(this.sortField, this.sortDirection);
            // remove prev sort markers
        }
        let headRow = event.target.parentElement;
        if (headRow !== null) {
            let headCells = headRow.querySelectorAll('th');
            headCells.forEach((cell) => {
                cell.classList.remove('sort', 'sort-asc', 'sort-desc');
            });
        }
        if (event && event.target) {
            event.target.classList.remove('sort-asc', 'sort-desc');
            event.target.classList.add('sort');
            if (this.sortDirection === 'asc') {
                event.target.classList.add('sort-asc');
            } else {
                event.target.classList.add('sort-desc');
            }
        }
        if (this.dataSource.dataFiltered) {
            this.renderBody(this.dataSource.dataFiltered);
        } else {
            this.renderBody(this.dataSource.data);
        }
    }

    changeFilter(event) {
        let filterValues = [];
        if (this.filters && this.filters.length > 0) {
            for (let filter of this.filters) {
                if (filter.value != null) {
                    filterValues.push(filter.value);
                }
            }
            this.dataSource.filter(filterValues);
        } else {
            this.dataSource.filter(event.detail.value);
        }
        this.renderBody(this.dataSource.dataFiltered);
    }

    bindHeadingEvents(row) {
        if (this.onhrowclick && typeof this.onhrowclick === 'function') {
            row.addEventListener('click', this.onhrowclick.bind(this));
        }
    }

    renderHeaderContents() {
        this.headEl = this.renderer.prepareTemplate(this.headTpl);

        let row = this.headEl.querySelector('tr') || this.renderer.createEl('tr');

        this.bindHeadingEvents(row);
        let sortField = this.getAttribute('sort-field');
        for (let field of this.dataSource.fields) {
            let headCell = this.renderer.createEl('th');
            if (typeof field == 'string') {
                headCell.textContent = field;
                headCell.setAttribute('data-sort', field);
            } else {
                if (field.title !== undefined) {
                    headCell.textContent = field.title;
                    headCell.setAttribute('data-sort', field.path);
                }
            }
            // display initial sort markerts
            if (sortField && (field.path === sortField || field === sortField)) {
                headCell.classList.add('sort');
                if (this.sortDirection === 'desc') {
                    headCell.classList.add('sort-desc');
                } else {
                    headCell.classList.add('sort-asc');
                }
            }
            row.appendChild(headCell);

        }
        this.dispatchEvent(new CustomEvent('headerRowRendered'));
        if (sortField) {
            row.addEventListener('click', this.changeSort.bind(this));
        }
        if (this.headEl.tagName === 'THEAD') {
            this.headEl.appendChild(row);
        } else {
            this.headEl.querySelector('thead').appendChild(row);
        }
        let tableEl = this.querySelector('table');
        if (tableEl !== null) {
            let headEl = tableEl.querySelector('thead');
            if (headEl != null) {
                this.headEl = headEl;
            } else {
                tableEl.appendChild(this.headEl);
            }
            this.dispatchEvent(new CustomEvent('headersRendered'));
        } else {
            console.error('Not found table el');
        }
    }

    async renderHeaders() {
        let context = this;
        this.headTpl = await this.renderer.mountTemplate(this.headTplPath, 'tableHeadTpl', this, context);
        this.renderHeaderContents();

    }

    bindRowEvents(row) {
        if (this.onrowclick && typeof this.onrowclick === 'function') {
            row.addEventListener('click', this.onrowclick.bind(this));
        }
    }

    renderRow(bodyEl, dataItem) {
        let row = this.renderer.createEl('tr');

        this.bindRowEvents(row);

        for (let field of this.dataSource.fields) {
            let cell = this.renderer.createEl('td');

            let value = this.specCellContents(field, dataItem, cell);
            this.specCellAtts(field, cell, value, dataItem);
            this.specRowAttrs(field, row, value, dataItem);

            row.appendChild(cell);
        }
        bodyEl.appendChild(row);
        this.dispatchEvent(new CustomEvent('bodyRowRendered'));
    }

    specCellContents(field, dataItem, cell) {
        let value = '';
        if (typeof field == 'string') {
            value = dataItem[field];
        } else {
            value = JSONPath.query(dataItem, field.path)[0];
            if (field.fmt && typeof field.fmt === 'function') {
                value = field.fmt.call(this, field, value, dataItem);
            }
        }
        if (value && !field.html) {
            cell.textContent = value;
        }
        if (value && field.html) {
            cell.innerHTML = value;
        }
        return value;
    }

    specCellAtts(field, cell, value, dataItem) {
        if (field.attr) {
            for (let attrName of Object.keys(field.attr)) {
                let attrValue = field.attr[attrName];
                if (typeof attrValue == 'string') {
                    cell.setAttribute(attrName, attrValue);
                } else if (typeof attrValue == 'function') {
                    cell.setAttribute(attrName, attrValue.call(this, field, value, dataItem));
                }
            }
        }
    }

    specRowAttrs(field, row, value, dataItem) {
        if (field.rowattr) {
            for (let attrName of Object.keys(field.rowattr)) {
                let attrValue = field.rowattr[attrName];
                if (typeof attrValue == 'string') {
                    row.setAttribute(attrName, attrValue);
                } else if (typeof attrValue == 'function') {
                    row.setAttribute(attrName, attrValue.call(this, field, value, dataItem));
                }
            }
        }
    }

    async render(data) {
        this.tpl = await this.renderer.mountTemplate(this.tplPath, 'tableTpl', this);
        this.innerHTML = '';
        let el = this.renderer.prepareTemplate(this.tpl);
        this.appendChild(el);
        if (this.dataSource && this.headers) {
            this.renderHeaders();
        }
        await this.renderBody(data, this.backPaged);

        this.renderTimest = Date.now();
    }

    async renderBody(data, backPaged) {
        let context = this;
        let bodyTpl = await this.renderer.mountTemplate(this.bodyTplPath, 'tableBodyTpl', this, context);
        this.bodyTpl = bodyTpl;
        let tbody = this.querySelector('tbody');
        if (tbody != null) {
            tbody.innerHTML = '';
        } else {
            let bodyEl = this.renderer.prepareTemplate(this.bodyTpl);
            this.querySelector('table').appendChild(bodyEl);
            tbody = this.querySelector('tbody');
        }
        if (this.pager) {
            let start = 0;
            if (this.pager.curPageNum > 1) {
                start = (this.pager.curPageNum - 1) * this.pager.perPage;
            }
            let end = start + this.pager.perPage;
            if (this.pager.perPage < this.totalResults) {
                end = data.length - 1;
            }
            if (this.pager.curPageNum === this.pager.totalPages) {
                end = (start + ((this.pager.perPage * this.pager.totalPages) - this.pager.totalResults)) - 1;
            }
            if (! backPaged) {
                for (let i = start; i < end; i++) {
                    if (data[i] !== undefined) {
                        this.renderRow(tbody, data[i]);
                    }
                }
            } else {
                for (let dataItem of data) {
                    this.renderRow(tbody, dataItem);
                }
            }
        } else {
            for (let dataItem of data) {
                this.renderRow(tbody, dataItem);
            }
        }
        this.dispatchEvent(new CustomEvent('bodyRendered'));


    }

    changePage(event) {
        let backPaged = event.detail.backPaged;
        let pageNum = event.detail.page;
        let detail = {
            backPaged: backPaged,
            page: pageNum
        };
        let sortField = event.detail.sortField;
        if (sortField) {
            detail.sortField = sortField;
            detail.sortDirection = event.detail.sortDirection || 'asc';
        }
        let sortDirection = event.detail.sortDirection;
        if (backPaged) {
            this.dataSource.loadData(detail);
        } else {
            this.renderData(event);
        }

    }

    renderData(event) {
        this.backPaged = event && event.detail.backPaged;
        if (! this.backPaged && this.getAttribute('sort-field')) { // initial sort
            this.dataSource.sort(this.sortField, this.sortDirection);
        }
        let data = event && event.detail.data || this.dataSource.data;
        if (this.renderTimest < 0) {
            this.render(data);
        } else {
            this.renderBody(data, this.backPaged);
            //this.renderTimest = Date.now();
        }
    }

    bindDataSource(dataSource) {
        if (dataSource) {
            this.dataSource = dataSource;
        }
        this.dataSource.addEventListener('dataLoaded', this.renderData.bind(this));
    }

    set dataSource(dataSource) {
        this._dataSource = dataSource;
    }

    get dataSource() {
        return this._dataSource;
    }

    get dsRef() {
        return this.getAttribute('dsref');
    }

    get pgRef() {
        return this.getAttribute('pgref');
    }

    bootstrap() {
        if (! this.dataSource) {
            if (this.dsRef) {
                if (window[this.dsRef]) {
                    if (typeof window[this.dsRef] === 'function') {
                        window[this.dsRef].call(this);
                    } else {
                        let dsEl = window[this.dsRef];
                        if (!customElements.get('gridy-data-source')) {
                            customElements.define('gridy-data-source', GridyDataSource);
                        }
                        dsEl.config();
                        this.dataSource = dsEl.dataSource;
                    }
                } else {
                    if (this.dsRef && typeof this.dsRef === 'string' && this.dsRef.indexOf('.') > 0) {
                        let pathTokens = this.dsRef.split('.');
                        let root = window[pathTokens[0]];
                        if (root) {
                            let ds = root[pathTokens[1]];
                            if (ds) {
                                this.dataSource = ds;
                            }
                        }
                    }
                }
            } else {
                // find data in markup and fill newly create dds
                this.dataFromHtml();
            }
        }
        if (this.dataSource) {
            this.bindDataSource();
        }
        if (this.dsRef) {
            let dsEl = window[this.dsRef];
            dsEl.loadDataRef();
        }
        if (this.pgRef) {
            this.pager = window[this.pgRef];
        }
        if (this.pager) {
            this.pager.addEventListener('pageChanged', this.changePage.bind(this));
        }
        if (this.filter) {
            this.filter.addEventListener('filterChanged', this.changeFilter.bind(this));
        }
        if (this.filters) {
            for (let filter of this.filters) {
                filter.addEventListener('filterChanged', this.changeFilter.bind(this));
            }
        }
        if (this.dataSource) {
            this.dataSource.init();
        }
        if (! customElements.get('gridy-pager')) {
            customElements.define('gridy-pager', GridyPager);
        }
    }

    dataFromHtml() {
        let rows = this.querySelectorAll('tbody tr');
        let fields = [];
        if (rows.length > 0) {
            let data = [];
            for (let row of rows) {
                let rowData = [];
                let cells = row.querySelectorAll('td');
                if (fields.length <= 0 && cells.length > 0) {
                    let i = 0;
                    for (let cell of cells) {
                        fields.push({ path: `$.[${i}]`, html: true});
                        i++;
                    }
                }
                for (let cell of cells) {
                    rowData.push(cell.innerHTML);
                }
                data.push(rowData);
            }
            let dataSource = new DataSourceLocal();
            dataSource.fields = fields;
            this.bindDataSource(dataSource);
            this.dataSource.loadData(data);
        }
    }

    useConfigEl() {
        if (! customElements.get('gridy-config')) {
            customElements.define('gridy-config', GridyConfig);
        }
        if (this.configEl && typeof this.configEl.configureElement === 'function') {
            this.configEl.configureElement(this);
        }
    }

    connectedCallback() {

        this.useConfigEl();
        Renderer.configureForElement(this);

        this.bootstrap();
    }
}