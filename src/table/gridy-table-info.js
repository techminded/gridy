
import { Renderer } from '../../complets/renderer.js';
import { GridyConfig } from '../gridy-config.js';
import { LANGS_BY_CODES } from "../gridy-config.js";


export class GridyTableInfo extends HTMLElement {

    //renderer: Renderer;
    //theme: string;

    //tplPath;
    //tpl;

    changePage(event) {
        this.renderData();
    }

    get tplPath() {
        return this.getAttribute('tpl-path') || this.basePath + '/theme/' + this.theme + '/table-info.tpl.html';
    }

    set tplPath(tplPath) {
        this.setAttribute('tpl-path', tplPath);
    }

    get noEntriesTplPath() {
        return this.getAttribute('noent-tpl-path');
    }

    set noEntriesTplPath(tplPath) {
        this.setAttribute('noent-tpl-path', tplPath);
    }

    get theme() {
        return this.getAttribute('theme') || 'default';
    }

    set theme(theme) {
        return this.setAttribute('theme', theme);
    }

    get basePath() {
        return this.getAttribute('base-path') || null;
    }

    set basePath(basePath) {
        this.setAttribute('base-path', basePath);
    }

    get configSl() {
        return this.getAttribute('configSl') || 'sk-config, gridy-config';
    }

    get configEl() {
        if (! this._configEl) {
            this._configEl = document.querySelector(this.configSl);
        }
        return this._configEl;
    }

    buildContext(event) {
        let from = (this.pager.curPageNum - 1) * this.pager.perPage;
        let to = from + this.pager.perPage;
        let total = 0;
        if (event && event.target && event.target.type === 'pageChanged') {
            total = this.pager.totalResults || 0;
        } else {
            total = this.dataSource.total || 0;
        }
        if (to > total) {
            to = total;
        }
        return {
            from: total === 0 ? 0 : from + 1,
            to: to,
            total: total
        };
    }

    renderData(event) {
        let path = this.tplPath;
        let id = 'tableInfoTpl';
        if (this.dataSource.total === 0 && (this.noEntriesTplPath || this.querySelector('#noEntriesInfoTpl'))) {
            path = this.noEntriesTplPath;
            id = 'noEntriesInfoTpl'
        }
        this.renderer.mountTemplate(path, id, this).then((templateEl) => {
            this.tpl = templateEl;
            let el = this.renderer.prepareTemplate(templateEl);

            let map = this.buildContext(event);
            let path = null;
            if (this.lang) {
                let locale = LANGS_BY_CODES[this.lang];
                if (locale === 'RU') {
                    path = this.basePath + '/theme/' + this.theme + '/i18n/gridy-i18n-ru_RU.json';
                }
                if (path) {
                    this.renderer.localizeEl(el, this.lang, path).then((translatedEl) => {
                        this.innerHTML = '';
                        this.insertAdjacentHTML('beforeend', this.renderer.templateEngine.render(translatedEl, map));
                        this.dispatchEvent(new CustomEvent('tableInfoRendered'));
                    });
                }
            }
            if (! this.lang || ! path) {
                this.innerHTML = '';
                this.insertAdjacentHTML('beforeend', this.renderer.templateEngine.render(el, map));
                this.dispatchEvent(new CustomEvent('tableInfoRendered'));
            }
        });
    }

    useConfigEl() {
        if (! customElements.get('gridy-config')) {
            customElements.define('gridy-config', GridyConfig);
        }
        if (this.configEl) {
            this.configEl.configureElement(this);
        }
    }

    connectedCallback() {

        this.useConfigEl();
        this.setAttribute('basepath', this.getAttribute('base-path')); // compatibility fix
        Renderer.configureForElement(this);

        if (this.dataSource) {
            this.dataSource.addEventListener('dataLoaded', this.renderData.bind(this));
        }
        if (this.pager) {
            this.pager.addEventListener('pageChanged', this.changePage.bind(this));
        }
    }
}