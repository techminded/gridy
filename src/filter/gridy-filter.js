
import { Renderer } from '../../complets/renderer.js';
import { GridyConfig } from '../gridy-config.js';




export class GridyFilter extends HTMLElement {


    //renderer: Renderer;
    //theme: string;

    //tplPath;
    //tpl;


    get tplPath() {
        return this.getAttribute('tpl-path') || this.basePath + '/theme/' + this.theme + '/filter.tpl.html';
    }

    set tplPath(tplPath) {
        this.setAttribute('tpl-path', tplPath);
    }

    get theme() {
        return this.getAttribute('theme') || 'default';
    }

    set theme(theme) {
        return this.setAttribute('theme', theme);
    }

    get basePath() {
        return this.getAttribute('basepath') || null;
    }

    set basePath(basePath) {
        this.setAttribute('basepath', basePath);
    }

    get input() {
        if (! this._input) {
            this._input = this.querySelector('input');
        }
        return this._input;
    }

    get value() {
        if (this.input) {
            return this.input.value;
        }
        return null;
    }

    changeFilter(event) {
        this.dispatchEvent(new CustomEvent('filterChanged', {
            detail: { value: event.target.value }
        }));
    }

    get configSl() {
        return this.getAttribute('configSl') || 'sk-config, gridy-config';
    }

    get configEl() {
        if (! this._configEl) {
            this._configEl = document.querySelector(this.configSl);
        }
        return this._configEl;
    }

    useConfigEl() {
        if (! customElements.get('gridy-config')) {
            customElements.define('gridy-config', GridyConfig);
        }
        if (this.configEl) {
            this.configEl.configureElement(this);
        }
    }

    connectedCallback() {

        this.useConfigEl();
        Renderer.configureForElement(this);

        if (this.dataSource) {
            this.dataSource.addEventListener('dataLoaded', (event) => {
            });
        }
        if (this.basePath) {
            let context = this;
            this.renderer.mountTemplate(this.tplPath, 'filterTpl', this, context).then((templateEl) => {
                this.tpl = templateEl;
                this.appendChild(this.renderer.prepareTemplate(this.tpl));
                if (this.dataSource) {
                    let input = this.querySelector('input');
                    if (input != null) {
                        input.addEventListener('input', this.changeFilter.bind(this));
                    }
                }
            });
        }
    }
}