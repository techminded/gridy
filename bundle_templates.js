
const fs = require('fs');

// + iterate directories in src/theme/
// + load ${theme}-templates.json from every theme
// + build bundle dist/${theme}.templates.html

// load templates from ${theme}-templates.js from impl classes
// provide examples with sync fetch load

const getDirectories = source =>
    fs.readdirSync(source, { withFileTypes: true })
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name);

const themePath = "src/theme";

function bundleTemplates(bundleType) {
    bundleType = bundleType ? bundleType : 'native';
    let dirs = getDirectories(themePath);
    for (let dir of dirs) {
        console.log('Building templates bundle for theme ' + themePath + '/' + dir, ' with type: ', bundleType);
        let bundle = `${dir}.tpls.${bundleType}.html`;
        let templateConfig = JSON.parse(fs.readFileSync(themePath + '/' + dir + '/' + dir + '-templates.json', 'utf8'));
        let bundles = Object.keys(templateConfig);
        let writeStream = fs.createWriteStream('dist/gridy-' + bundle);
        for (let templateId of bundles) {
            let templatePath = templateConfig[templateId];
            let content = fs.readFileSync(templatePath, 'utf8');
            if (bundleType === 'hg') {
                writeStream.write(`
<script type="text/x-handlebars-template" id="${templateId}">
${content}
</script>`
                );
            } else {
                writeStream.write(`
<template id="${templateId}">
${content}
</template>`
                );
            }
        }
        writeStream.end();
    }
}

bundleTemplates();
bundleTemplates('hg');